<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    function owner() {
        return $this->belongsTo('App\User', 'owner')->first();
    }

    function statusText() {
        $return = '';
        foreach(CONFIG('product.status.merchant') as $key => $value){
            if ($this->status == $value['value']) {
                $return = $value['text'];
                break;
            }
        }

        return $return;
    }
}
