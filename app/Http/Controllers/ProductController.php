<?php

namespace App\Http\Controllers;

use App\Product;
use App\Offer;
use App\Brand;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);

        $data = array(
            'products' => $products
        );
        return view('product.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::orderBy('name')->get();
        $categories = Category::orderBy('name')->get();

        $data = array(
            'brands' => $brands,
            'categories' => $categories
        );
        return view('product.update')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validator());
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $product = New Product();
        $product->name = $request->get('name');
        $product->brand_id = $request->get('brand_id');
        $product->category_id = $request->get('category_id');
        $product->status = $request->get('status');
        $product->description = $request->get('description');

        $product->save();

        return redirect('/product')->with('status-success', 'Product updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $brands = Brand::orderBy('name')->get();
        $categories = Category::orderBy('name')->get();

        $data = array(
            'product' => $product,
            'brands' => $brands,
            'categories' => $categories
        );

        return view('product.update')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validator($id));
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $product = Product::find($id);

        $product->name = $request->get('name');
        $product->brand_id = $request->get('brand_id');
        $product->category_id = $request->get('category_id');
        $product->status = $request->get('status');
        $product->description = $request->get('description');

        $product->save();

        return redirect('/product')->with('status-success', 'Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product){
            $name = $product->name;
            $product->delete();
            return redirect('/product')->with("status-success", "<b>".$name."</b> deleted.");
        }else{
            return redirect('/product')->with("status-danger", "Data not exists.");
        }
    }

    public function create_offer($id) {
        $product = Product::find($id);

        $data = array(
            'product' => $product
        );
        return view('product.offer-update')->with($data);
    }

    public function store_offer(Request $request, $id) {
        $validator = Validator::make($request->all(), $this->offer_validator());
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $product = Product::find($id);
        $offer = new Offer();
        $offer->product_id = $product->id;
        $offer->merchant_id = 1;
        $offer->name = $request->get('name');
        $offer->buying_price = $request->get('buying_price');
        $offer->price = $request->get('price');
        $offer->special_price = $request->get('special_price');
        $offer->status = $request->get('status');
        $offer->description = $request->get('description');
        $offer->note = $request->get('note');

        $offer->save();

        return redirect('/product')->with('status-success', 'Offer updated!');
    }

    public function edit_offer() {

    }

    public function update_offer() {

    }

    public function validator($id = '') {
        return [
            'name' => 'unique:products'.($id != '' ? ',name,'.$id : ''),
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:1024|dimensions:ratio=1/1',
        ];
    }

    public function offer_validator() {
        return [
            'buying_price' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:0',
            'special_price' => 'numeric|min:0',
        ];
    }
}
