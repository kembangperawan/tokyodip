<?php

namespace App\Http\Controllers;

use App\Merchant;
use App\User;
use Illuminate\Http\Request;
use Validator;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merchants = Merchant::paginate(10);

        $data = array(
            'merchants' => $merchants
        );
        return view('merchant.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('name')->get();

        $data = array(
            'users' => $users
        );
        return view('merchant.update')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validator());
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $merchant = New Merchant();
        $merchant->name = $request->get('name');
        $merchant->owner = $request->get('owner');
        $merchant->description = $request->get('description');
        $merchant->status = $request->get('status');

        $merchant->save();

        return redirect('/merchant')->with('status-success', 'Merchant updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $merchant = Merchant::find($id);
        $users = User::orderBy('name')->get();

        $data = array(
            'merchant' => $merchant,
            'users' => $users
        );

        return view('merchant.update')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validator($id));
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $merchant = Merchant::find($id);

        $merchant->name = $request->get('name');
        $merchant->owner = $request->get('owner');
        $merchant->description = $request->get('description');
        $merchant->status = $request->get('status');

        $merchant->save();

        return redirect('/merchant')->with('status-success', 'Merchant updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $merchant = Merchant::find($id);
        if ($merchant){
            $name = $merchant->name;
            $merchant->delete();
            return redirect('/merchant')->with("status-success", "<b>".$name."</b> deleted.");
        }else{
            return redirect('/merchant')->with("status-danger", "Data not exists.");
        }
    }

    public function validator($id = '') {
        return [
            'name' => 'unique:merchants'.($id != '' ? ',name,'.$id : ''),
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:1024|dimensions:ratio=1/1',
        ];
    }
}
