<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use Validator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::paginate(10);

        $data = array(
            'brands' => $brands
        );
        return view('brand.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brand.update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validator());
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $brand = New Brand();
        $brand->name = $request->get('name');

        $brand->save();

        return redirect('/brand')->with('status-success', 'Brand updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);

        $data = array(
            'brand' => $brand
        );

        return view('brand.update')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validator($id));
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $brand = Brand::find($id);

        $brand->name = $request->get('name');

        $brand->save();

        return redirect('/brand')->with('status-success', 'Brand updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        if ($brand){
            $name = $brand->name;
            $brand->delete();
            return redirect('/brand')->with("status-success", "<b>".$name."</b> deleted.");
        }else{
            return redirect('/brand')->with("status-danger", "Data not exists.");
        }
    }

    public function validator($id = '') {
        return [
            'name' => 'unique:brands'.($id != '' ? ',name,'.$id : ''),
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:1024|dimensions:ratio=1/1',
        ];
    }
}
