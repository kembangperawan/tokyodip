<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template.default');
});

Route::resources([
    'product' => 'ProductController',
    'brand' => 'BrandController',
    'category' => 'CategoryController',
    'merchant' => 'MerchantController',
    'user' => 'UserController',
]);

Route::get('/product/{id}/offer', 'ProductController@create_offer');
Route::post('/product/{id}/offer', 'ProductController@store_offer');
Route::get('/offer/{id}', 'ProductController@edit_offer');
Route::post('/offer/{id}', 'ProductController@update_offer');