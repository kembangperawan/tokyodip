@extends('template.default')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Product</h1>
</div>
<div class="row">
    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <b>Error accured.</b>
            <ul>
        @foreach ($errors->all() as $message)
                <li>{{$message}}</li>
        @endforeach
            </ul>
        </div>
        @endif
        <form method="post" action="{{ url(isset($offer) ? '/offer/' . $offer->id : '/product/'.$product->id.'/offer') }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="{{ isset($offer) ? 'PUT': 'POST' }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" required class="form-control" value="{{ isset($product) ? $product->name : old('name') }}" />
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control">{{ isset($offer) ? $offer->description : old('description') }}</textarea>
            </div>
            <div class="form-group">
                <label>Buying Price</label>
                <input type="text" name="buying_price" required class="form-control" value="{{ isset($offer) ? $offer->buying_price : old('buying_price') }}" />
            </div>
            <div class="form-group">
                <label>Sell Price</label>
                <input type="text" name="price" required class="form-control" value="{{ isset($offer) ? $offer->price : old('price') }}" />
            </div>
            <div class="form-group">
                <label>Special Price</label>
                <input type="text" name="special_price" class="form-control" value="{{ isset($offer) ? $offer->special_price : old('special_price') }}" />
            </div>
            <div class="form-group">
                <label>Special Price Start Date</label>
                <input type="text" name="special_price_start_date" class="form-control" value="{{ isset($offer) ? $offer->special_price_start_date : old('special_price_start_date') }}" />
            </div>
            <div class="form-group">
                <label>Special Price End Date</label>
                <input type="text" name="special_price_end_date" class="form-control" value="{{ isset($offer) ? $offer->special_price_end_date : old('special_price_end_date') }}" />
            </div>
            <div class="form-group">
                <label>Note</label>
                <textarea name="note" class="form-control">{{ isset($offer) ? $offer->note : old('note') }}</textarea>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    @foreach(CONFIG('product.status.offer') as $key => $value)
                    <option value="{{ $value['value']}}" {{isset($offer) && $offer->status == $value['value'] ? 'selected=selected' : ''}}>{{ $value['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Logo</label>
                <input type="file" name="file" id="file" />
                <p>*Logo must be 1:1 ratio. eg : 100px x 100px</p>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-default" href="{{ URL('/offer') }}">Cancel</a>
            </div>
        </form>

        @if(isset($offer))
        <form method="POST" action="{{ URL('/offer/'.$offer->id) }}" accept-charset="UTF-8"
        onsubmit="return window.confirm('Delete this data?');">
            <input name="_method" type="hidden" value="DELETE" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn text-danger" name='Delete' class="link" value="Delete"/>
        </form>
        @endif
    </div>
</div>
@stop