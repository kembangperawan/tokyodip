@extends('template.default')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">User</h1>
</div>
<div class="row">
    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <b>Error accured.</b>
            <ul>
        @foreach ($errors->all() as $message)
                <li>{{$message}}</li>
        @endforeach
            </ul>
        </div>
        @endif
        <form method="post" action="{{ url(isset($user) ? '/user/' . $user->id : '/user') }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="{{ isset($user) ? 'PUT': 'POST' }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" required class="form-control" value="{{ isset($user) ? $user->name : old('name') }}" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" required class="form-control" value="{{ isset($user) ? $user->email : old('email') }}" />
            </div>
            @if(!isset($user))
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" required class="form-control"  />
            </div>
            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-default" href="{{ URL('/user') }}">Cancel</a>
            </div>
        </form>

        @if(isset($user))
        <form method="POST" action="{{ URL('/user/'.$user->id) }}" accept-charset="UTF-8"
        onsubmit="return window.confirm('Delete this data?');">
            <input name="_method" type="hidden" value="DELETE" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn text-danger" name='Delete' class="link" value="Delete"/>
        </form>
        @endif
    </div>
</div>
@stop