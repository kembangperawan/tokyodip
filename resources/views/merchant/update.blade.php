@extends('template.default')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Merchant</h1>
</div>
<div class="row">
    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <b>Error accured.</b>
            <ul>
        @foreach ($errors->all() as $message)
                <li>{{$message}}</li>
        @endforeach
            </ul>
        </div>
        @endif
        <form method="post" action="{{ url(isset($merchant) ? '/merchant/' . $merchant->id : '/merchant') }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="{{ isset($merchant) ? 'PUT': 'POST' }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label>Owner</label>
                <select name="owner" class="form-control">
                    @foreach($users as $key => $value)
                    <option value="{{ $value->id}}" {{isset($merchant) && $merchant->owner == $value->id ? 'selected=selected' : ''}}>{{ $value->name.'('.$value->email.')'}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" required class="form-control" value="{{ isset($merchant) ? $merchant->name : old('name') }}" />
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control">{{ isset($merchant) ? $merchant->description : old('description') }}</textarea>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    @foreach(CONFIG('product.status.merchant') as $key => $value)
                    <option value="{{ $value['value']}}" {{isset($merchant) && $merchant->status == $value['value'] ? 'selected=selected' : ''}}>{{ $value['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Logo</label>
                <input type="file" name="file" id="file" />
                <p>*Logo must be 1:1 ratio. eg : 100px x 100px</p>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-default" href="{{ URL('/merchant') }}">Cancel</a>
            </div>
        </form>

        @if(isset($merchant))
        <form method="POST" action="{{ URL('/merchant/'.$merchant->id) }}" accept-charset="UTF-8"
        onsubmit="return window.confirm('Delete this data?');">
            <input name="_method" type="hidden" value="DELETE" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn text-danger" name='Delete' class="link" value="Delete"/>
        </form>
        @endif
    </div>
</div>
@stop