@extends('template.default')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Merchant</h1>
</div>
<div class="row">
    <div class="col-12">
        <div>
            <a href="{{ URL('/merchant/create') }}" class="btn btn-primary">Create New</a>
        </div>
        <br />
        @include('template.alert', 
        [
        'status_success' => session('status-success'),
        'status_danger' => session('status-danger')
        ])
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="font-weight-bold text-primary">Merchant List</h6>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Owner</th>
                        <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($merchants as $key => $value)
                        <tr>
                            <td><a href="{{ URL('/merchant/'.$value->id.'/edit')}}">{{ $value->id }}</a></td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->owner()->name }}</td>
                            <td>{{ $value->statusText() }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $merchants->links() }}
            </div>
        </div>
    </div>
</div>
@stop