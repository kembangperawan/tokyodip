@extends('template.default')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Category</h1>
</div>
<div class="row">
    <div class="col-12">
        <div>
            <a href="{{ URL('/category/create') }}" class="btn btn-primary">Create New</a>
        </div>
        <br />
        @include('template.alert', 
        [
        'status_success' => session('status-success'),
        'status_danger' => session('status-danger')
        ])
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="font-weight-bold text-primary">Category List</h6>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $key => $value)
                        <tr>
                            <td><a href="{{ URL('/category/'.$value->id.'/edit')}}">{{ $value->id }}</a></td>
                            <td>{{ $value->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $categories->links() }}
            </div>
        </div>
    </div>
</div>
@stop