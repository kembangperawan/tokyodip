@extends('template.default')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Category</h1>
</div>
<div class="row">
    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <b>Error accured.</b>
            <ul>
        @foreach ($errors->all() as $message)
                <li>{{$message}}</li>
        @endforeach
            </ul>
        </div>
        @endif
        <form method="post" action="{{ url(isset($category) ? '/category/' . $category->id : '/category') }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="{{ isset($category) ? 'PUT': 'POST' }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" required class="form-control" value="{{ isset($category) ? $category->name : old('name') }}" />
            </div>
            <div class="form-group">
                <label>Logo</label>
                <input type="file" name="file" id="file" />
                <p>*Logo must be 1:1 ratio. eg : 100px x 100px</p>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-default" href="{{ URL('/category') }}">Cancel</a>
            </div>
        </form>

        @if(isset($category))
        <form method="POST" action="{{ URL('/category/'.$category->id) }}" accept-charset="UTF-8"
        onsubmit="return window.confirm('Delete this data?');">
            <input name="_method" type="hidden" value="DELETE" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn text-danger" name='Delete' class="link" value="Delete"/>
        </form>
        @endif
    </div>
</div>
@stop