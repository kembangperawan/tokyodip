<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id');
            $table->integer('merchant_id');
            $table->string('name');
            $table->text('images')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price')->default(0);
            $table->decimal('special_price')->default(0);
            $table->decimal('buying_price')->default(0);
            $table->datetime('special_price_start_date')->nullable();
            $table->datetime('special_price_end_date')->nullable();
            $table->smallInteger('status')->default(1);
            $table->string('note')->nullable();
            $table->integer('created_by')->default(1);
            $table->integer('updated_by')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
