<?php

return [
    'product' => [
        array(
            'value' => 0,
            'text' => 'NONACTIVE'
        ),
        array(
            'value' => 1,
            'text' => 'ACTIVE'
        )
    ],
    'offer' => [
        array(
            'value' => 0,
            'text' => 'NONACTIVE'
        ),
        array(
            'value' => 1,
            'text' => 'ACTIVE'
        )
    ],
    'merchant' => [
        array(
            'value' => 0,
            'text' => 'NONACTIVE'
        ),
        array(
            'value' => 1,
            'text' => 'REGISTERED'
        ),
        array(
            'value' => 2,
            'text' => 'IN REVIEW'
        ),
        array(
            'value' => 3,
            'text' => 'ACTIVE'
        ),
        array(
            'value' => 4,
            'text' => 'REJECTED'
        ),
    ]
];